# qml-watcher-display

A simple program to display a QML file, which is to be used together with <a href="https://gitlab.com/rszibele/qml-watche#readmer">qml-watcher</a>.

## Prerequisites

You must have stack and the Qt development packages installed to be able to build qml-watcher-display.

## Build Instructions

````
stack build
stack install
````
