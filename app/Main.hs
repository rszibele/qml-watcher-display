module Main where

import Graphics.QML
import System.Environment
import System.Exit

main :: IO ()
main = do
    args <- getArgs
    case args of
        [] -> die "No QML file supplied"
        xs -> do
            let filePath = head args
            mainClass <- newClass []
            ctx <- newObject mainClass ()
            runEngineLoop defaultEngineConfig {
                initialDocument = fileDocument filePath,
                contextObject = Just $ anyObjRef ctx}